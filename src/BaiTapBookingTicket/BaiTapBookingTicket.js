import React, { Component } from "react";
import "./BaiTapBookingTickKet.css";
import ThongTinDatVe from "./ThongTinDatVe";
import danhSachGheData from "../Data/danhSachGhe.json";
import HangGhe from "./HangGhe";
export default class BaiTapBookingTicket extends Component {
  renderHangGhe = () => {
    return danhSachGheData.map((soHang, index) => {
      return <HangGhe key={index} soHang={soHang} soHangGhe={index} />;
    });
  };

  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          position: "fixed",
          width: "100%",
          height: "100%",
          backgroundImage: "url(./img/bookingTicket/bgmovie.jpg)",
          backgroundSize: "cover",
        }}
      >
        <div
          style={{
            position: "fixed",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,0.7)",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 text-center">
                <div className="text-warning display-4 ">ĐẶT VÉ XEM PHIM</div>
                <p className="mt-3 mb-0 text-light">Màn hình</p>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                  }}
                >
                  <div className="screen"></div>
                </div>
                <div className="mt-4">{this.renderHangGhe()}</div>
              </div>
              <div className="col-4">
                <div className="text-light" style={{ fontSize: "35px" }}>
                  DANH SÁCH GHẾ BẠN CHỌN
                </div>
                <ThongTinDatVe />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
