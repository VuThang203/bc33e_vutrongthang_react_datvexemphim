import { DAT_GHE, HUY_GHE } from "../constants/BaiTapDatVeConstants";

const initialState = {
  danhSachGheDangDat: [],
};

const BaiTapDatVeReducer = (state = initialState, action) => {
  switch (action.type) {
    case DAT_GHE: {
      let danhSachGheUpdate = [...state.danhSachGheDangDat];
      let index = danhSachGheUpdate.findIndex(
        (item) => item.soGhe === action.ghe.soGhe
      );
      if (index !== -1) {
        danhSachGheUpdate.splice(index, 1);
      } else {
        danhSachGheUpdate.push(action.ghe);
      }
      state.danhSachGheDangDat = danhSachGheUpdate;
      return { ...state };
    }
    case HUY_GHE: {
      let danhSachGheUpdate = [...state.danhSachGheDangDat];
      let index = danhSachGheUpdate.findIndex(
        (item) => item.soGhe === action.soGhe
      );
      if (index !== -1) {
        danhSachGheUpdate.splice(index, 1);
      }
      state.danhSachGheDangDat = danhSachGheUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default BaiTapDatVeReducer;
