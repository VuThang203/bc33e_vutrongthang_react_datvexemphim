import React, { Component } from "react";
import BaiTapDatVeReducer from "./redux/reducer/BaiTapDatVeReducer";
import { connect } from "react-redux";
import { DAT_GHE } from "./redux/constants/BaiTapDatVeConstants";
import { datGheAction } from "./redux/action/BaiTapDatVeAction";

class HangGhe extends Component {
  renderSoGhe = () => {
    return this.props.soHang.danhSachGhe.map((item, index) => {
      let gheDaDat = "";
      let disabled = false;
      if (item.daDat) {
        gheDaDat = "gheDuocChon";
        disabled = true;
      }

      let cssGheDangDat = "";
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === item.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = "gheDangChon";
      }
      return (
        <button
          onClick={() => {
            this.props.datGhe(item);
          }}
          disabled={disabled}
          className={`ghe ${gheDaDat} ${cssGheDangDat}`}
          key={index}
        >
          {item.soGhe}
        </button>
      );
    });
  };

  renderSoHang = () => {
    return this.props.soHang.danhSachGhe.map((item, index) => {
      return (
        <button
          key={index}
          style={{ fontSize: "20px" }}
          className="rowNumber mb-3"
        >
          {item.soGhe}
        </button>
      );
    });
  };

  renderHang = () => {
    if (this.props.soHangGhe === 0) {
      return this.renderSoHang();
    } else {
      return (
        <div>
          {this.props.soHang.hang}
          {this.renderSoGhe()}
        </div>
      );
    }
  };

  render() {
    return (
      <div className="text-warning ">
        <span>{this.renderHang()}</span>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BaiTapDatVeReducer.danhSachGheDangDat,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    datGhe: (ghe) => {
      dispatch(datGheAction(ghe));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
