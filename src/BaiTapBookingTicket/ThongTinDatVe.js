import React, { Component } from "react";
import BaiTapDatVeReducer from "./redux/reducer/BaiTapDatVeReducer";
import { connect } from "react-redux";
import { huyGheAction } from "./redux/action/BaiTapDatVeAction";

class ThongTinDatVe extends Component {
  render() {
    return (
      <div>
        <div className="mt-5 d-flex" style={{ fontSize: 20 }}>
          <button className="gheDuocChon"></button>
          <span className="text-light">Ghế đã đặt</span>

          <button className="gheDangChon "></button>
          <span className="text-light">Ghế đang chọn</span>

          <button className="ghe " style={{ marginLeft: 0 }}></button>
          <span className="text-light">Ghế chưa đặt</span>
        </div>

        <div className="mt-5">
          <table className="table" border="2">
            <thead>
              <tr className="text-light">
                <th>Số ghế</th>
                <th>Giá</th>
                <th>Huỷ</th>
              </tr>
            </thead>
            <tbody>
              {this.props.danhSachGheDangDat.map((item, index) => {
                return (
                  <tr key={index} className="text-warning">
                    <td>{item.soGhe}</td>
                    <td>{item.gia.toLocaleString()}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.dispatch(huyGheAction(item.soGhe));
                        }}
                        className="text-danger"
                      >
                        X
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot>
              <tr>
                <td></td>
                <td className="text-light">Tổng tiền</td>
                <td className="text-warning">
                  {this.props.danhSachGheDangDat
                    .reduce((tongTien, item, index) => {
                      return (tongTien += item.gia);
                    }, 0)
                    .toLocaleString()}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BaiTapDatVeReducer.danhSachGheDangDat,
  };
};

export default connect(mapStateToProps)(ThongTinDatVe);
